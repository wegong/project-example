#ifndef MODELHEADERDEF
#define MODELHEADERDEF

#include <fstream> // file streaming for input file
#include <armadillo> // armadillo library

class Model
{
public:

  template <typename T>
  void read_line(T*, std::ifstream&);
  void read_line(int*, std::ifstream&);
  void read_input(const std::string&);

  void assemble ();
  void apply_BC ();
  void solve ();
  void compute_reaction ();
  void print_results(const std::string & path,
		     const std::string & fname);

private:
	// Struct to store all input parameters from file ( extracted by read_input() function)
	
    int Nel,Nen,Nnp,n; // Number of elements, nodes-per-element, nodes, dofs per node
    arma::mat nodes; // Coordinates of all nodes
    arma::imat elements; // Node index pair from every element
    arma::vec E, A; // Young modulus and cross section of every element
    arma::imat pli, pdi; // constrained dofs matrix (1 -> constrained / 0 -> free) 
    arma::mat pl, pd; // load and displacement boundary conditions for every dof
	
	// Struct to store all vectors and matrices of the 2d truss stiffness model. 
	
    arma::mat K,K_f; // Final stiffness matrix + unconstrained stiffness matrix (floating).
    arma::vec f,d,r; // Load + displacement + reaction.
	
};

#endif
