#include <iostream>

int main()
{
  std::cout << "start dummy test" << std::endl;
  
  /*
  if (some-condition) {
    std::cerr << "test failed" << std::endl;
    return 1; // failure
  }
  */

  // all tests passed: success
  std::cout << "end dummy test" << std::endl;
  return 0; // success
}
